import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Button from './component/Button'
import Select from './component/Select'
class App extends Component {
  render() {
    return (
      <div className="App">
        <Button />
        <Select />
      </div>
    );
  }
}

export default App;
