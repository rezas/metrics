import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'

const styles = theme => ({
  button: {
    margin: theme.spacing.unit
  },
  input: {
    display: 'none'
  }
})

class ContainedButtons extends Component {
  onClick1 = (e) => {
    console.log(e.target)
  }

  onClick2 = (e) => {
    console.log('I\'m the Primary', e.target)
  }

  render() {
    const {classes} = this.props
    return (
      <div>
        <Button
          variant="contained"
          className={classes.button}
          onClick={this.onClick1}
        >
          Default
        </Button>
        <Button
          variant="contained"
          color="primary"
          className={classes.button}
          onClick={this.onClick2}
        >
          Primary
        </Button>
      </div>
    )
  }
}

ContainedButtons.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(ContainedButtons)
